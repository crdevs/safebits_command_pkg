<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections you wish
    | to use as your default connection for the command migrations, seeders,
    | models and so on.
    |
    */

    'connection' => env('DB_CONNECTION_COMMAND', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Commands Configurations
    |--------------------------------------------------------------------------
    |
    | Here you may specify the fields that are necessary for the commands to
    | be executed
    |
    */
    'server' => env('SERVER_ID'),

    /*
    |--------------------------------------------------------------------------
    | Scheduled Commands Setup
    |--------------------------------------------------------------------------
    |
    | Here you may specify the setup for all scheduled commands defined in
    | the project
    |
    */
    'scheduled_commands' => [

        'sb:first-scheduled-command-signature' => [
            'method' => 'everyMinute',
            'frequency' => null,
            'executions' => [
                ['first-parameter-key' => 'first-parameter-value'],
                ['second-parameter-key' => 'second-parameter-value'],
            ]
        ],

        'sb:second-scheduled-command-signature' => [
            'method' => 'cron',
            'frequency' => '0 */6 * * *',
            'executions' => [
                ['first-parameter-key' => 'first-parameter-value'],
                ['second-parameter-key' => 'second-parameter-value'],
            ]
        ],
    ]

];
