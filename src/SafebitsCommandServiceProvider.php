<?php

namespace Safebits\Command;

use Illuminate\Support\ServiceProvider;
use Safebits\Command\Command\MigrateCommand;

/**
 * Class SafebitsCommandServiceProvider
 * @package Safebits\Command
 */
class SafebitsCommandServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Registers provider commands
        if ($this->app->runningInConsole()) {
            $this->commands([
                MigrateCommand::class,
            ]);
        }

        // Publishes config file to local configuration path
        $this->publishes([
            __DIR__ . '/Config/safebits_command.php' => config_path('safebits_command.php'),
        ]);
    }
}
