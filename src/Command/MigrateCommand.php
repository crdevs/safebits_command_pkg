<?php

namespace Safebits\Command\Command;

use Illuminate\Console\Command;
use Safebits\Command\Database\Seeders\DatabaseSeeder;

/**
 * Class MigrateCommand
 * @package Safebits\Command\Command
 */
class MigrateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sb:migrate-command {--refresh} {--seed}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs Safebits Command migrations and seeders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Laravel does not allow full paths when executing migrations
        $fullPath = dirname(__DIR__) . "/Database/Migrations";
        $migrationsPath = str_replace(base_path(), '', $fullPath);

        //Checks if refresh was requested
        if ($this->hasOption('refresh') && $this->option('refresh')) {
            $migrationCommand = 'migrate:refresh';
        } else {
            $migrationCommand = 'migrate';
        }

        //Connection is required in order to create migrations table
        $connection = config('safebits_command.connection');

        \Schema::connection($connection)->disableForeignKeyConstraints();

        //Executes migrations
        \Artisan::call($migrationCommand, array('--path' => $migrationsPath, '--force' => true, '--database' => $connection));

        //Checks if seeders execution was requested, and if so, those seeders are executed
        if ($this->hasOption('seed') && $this->option('seed')) {
            \Artisan::call('db:seed', array('--class' => DatabaseSeeder::class, '--force' => true));
        }

        \Schema::connection($connection)->enableForeignKeyConstraints();
    }
}
