<?php

namespace Safebits\Command\Command;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Safebits\Command\Exceptions\JobProcessingException;
use Illuminate\Console\Parser;
use Safebits\Command\Models\Command as SBCommandModel;

/**
 * Class SBCommand
 * @package Safebits\Command\Command
 */
abstract class SBCommand extends Command
{
    /**
     * @var null|string
     */
    public $childClassName = null;

    /**
     * @var bool
     */
    public $scheduled;

    /**
     * SBCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->childClassName = get_class($this);
    }

    /**
     * Execute the console command.
     * @throws \Exception
     */
    public final function handle()
    {
        $enableForce = false;
        if ($this->hasOption('force') && $this->option('force')) {
            $enableForce = true;
        }

        $baseCommandQuery = (new SBCommandModel)->where(['className' => $this->childClassName]);

        if ($this->scheduled) {
            $commandExecutionParameters = $this->getCommandExecutionParameters(false);

            if (count($commandExecutionParameters) > 0) {
                $baseCommandQuery = $baseCommandQuery->where('parameters', json_encode($commandExecutionParameters));
            }
        }

        $command = $baseCommandQuery->first();

        // If Command is not defined
        if (!$command) {
            throw new JobProcessingException("The job '" . $this->childClassName . "' is not defined in 'sb_command'.");
        }

        if (!$command->serverId) {
            throw new JobProcessingException("The Job " . $command->name . " does not have an execution server configured.");
        }

        if ($this->checkCommandExecution($command)) {

            // If Command is inactive
            if (!$command->isActive && !$enableForce) {
                return;
            }

            // If Command is already running
            if($command->isRunning){
                $command->skippedExecutions += 1;
                $command->save();

                // Manage tolerance execution
                if ($command->skippedExecutions > $command->executionsTolerance) {
                    throw new JobProcessingException("The Job " . $command->name . " hasn't run for {$command->skippedExecutions} execution(s).");
                }
                return;
            }

            if (!$this->checkCommandParameters()) {
                throw new JobProcessingException("Missing parameters in job " . $command->name);
            }

            // Start running command
            $command->isRunning = true;
            $command->lastRunDate = Carbon::now();
            $command->save();

            try {
                $this->executeCommand();
            } catch (\Exception $exception) {
                throw $exception;
            } finally {
                $command->refresh();
                $command->isRunning = false;
                $command->skippedExecutions = 0;
                $command->save();
            }
        }
    }

    /**
     * @param SBCommandModel $command
     * @return bool
     */
    private function checkCommandExecution(SBCommandModel $command)
    {
        $localServerId = config('safebits_command.server', null);
        return $command->serverId == $localServerId && !$command->halt && $this->checkCommandParameters();
    }

    /**
     * @return bool
     */
    private function checkCommandParameters()
    {
        if ($this->scheduled) {
            $parametersList = $this->getCommandParametersList();
            $commandExecutionParameters = $this->getCommandExecutionParameters();

            return array_diff($parametersList, $commandExecutionParameters) == [] && array_diff($commandExecutionParameters, $parametersList) == [];
        } else {
            return true;
        }
    }

    /**
     * @return array
     */
    private function getCommandParametersList()
    {
        $signature = $this->signature;
        [$name, $arguments, $options] = Parser::parse($signature);

        $argumentsList = [];
        foreach ($arguments as $argument) {
            $argumentsList[] = $argument->getName();
        }

        return $argumentsList;
    }

    /**
     * @param bool $onlyParametersNames
     * @return array
     */
    private function getCommandExecutionParameters($onlyParametersNames = true)
    {
        $commandExecutionParameters = $this->arguments();
        unset($commandExecutionParameters['command']);

        if ($onlyParametersNames) {
            return array_keys($commandExecutionParameters);
        } else {
            return $commandExecutionParameters;
        }
    }

    /**
     *
     */
    public function checkHaltFlag()
    {
        $commandExecutionParameters = $this->getCommandExecutionParameters(false);
        $command = (new SBCommandModel)->where(['className' => $this->childClassName, 'parameters' => json_encode($commandExecutionParameters)])->first();
        if ($command->halt) {
            $command->skippedExecutions = 0;
            $command->isRunning = false;
            $command->halt = false;
            $command->save();
            exit("Terminate $command->name job because the halt flag is up.");
        }
    }

    /**
     * @return mixed
     */
    public abstract function executeCommand();
}
