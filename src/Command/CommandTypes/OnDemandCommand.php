<?php

namespace Safebits\Command\Command\CommandTypes;

use Safebits\Command\Command\SBCommand;

/**
 * Class OnDemandCommand
 * @package Safebits\Command\Command\CommandTypes
 */
abstract class OnDemandCommand extends SBCommand
{
    /**
     * @var bool
     */
    public $scheduled = false;
}
