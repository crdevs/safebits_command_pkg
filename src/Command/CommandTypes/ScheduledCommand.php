<?php

namespace Safebits\Command\Command\CommandTypes;

use Safebits\Command\Command\SBCommand;

/**
 * Class ScheduledCommand
 * @package Safebits\Command\Command\CommandTypes
 */
abstract class ScheduledCommand extends SBCommand
{
    /**
     * @var bool
     */
    public $scheduled = true;
}
