<?php

namespace Safebits\Command\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Safebits\Command\Models\CMDModel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Command\Models\CMDModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Command\Models\CMDModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Command\Models\CMDModel query()
 * @mixin \Eloquent
 */
class CMDModel extends Model
{
    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'createdAt';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'updatedAt';

    /**
     * Connection to master data base
     * @var $connection
     */
    protected $connection;

    /**
     * CMDModel constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->connection = config('safebits_command.connection', 'mysql');
    }
}
