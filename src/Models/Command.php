<?php

namespace Safebits\Command\Models;

/**
 * Safebits\Command\Models\Command
 *
 * @property int $commandId
 * @property string $name
 * @property string $className
 * @property string $signature
 * @property string $serverId
 * @property int $isActive
 * @property int $skippedExecutions
 * @property int $executionsTolerance
 * @property int $isRunning
 * @property int $halt
 * @property int $scheduled
 * @property mixed|null $parameters
 * @property string|null $lastRunDate
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Command\Models\Command newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Command\Models\Command newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Command\Models\Command query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Command\Models\Command whereClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Command\Models\Command whereCommandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Command\Models\Command whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Command\Models\Command whereHalt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Command\Models\Command whereScheduled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Command\Models\Command whereParameters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Command\Models\Command whereIsRunning($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Command\Models\Command whereLastRunDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Command\Models\Command whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Command\Models\Command whereSignature($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Command\Models\Command whereServerId($value)
 * @mixin \Eloquent
 */
class Command extends CMDModel
{
    /**
     * @var string
     */
    protected $primaryKey = 'commandId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * Command constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable('sys_commands');
    }

    public function halt()
    {

    }
}
