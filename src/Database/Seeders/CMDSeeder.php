<?php

namespace Safebits\Command\Database\Seeders;

use Illuminate\Database\Seeder;

/**
 * Class CMDSeeder
 * @package Safebits\Command\Database\Seeders
 */
class CMDSeeder extends Seeder
{
    /**
     * @var string
     */
    protected $connection;

    /**
     * CommonSeeder constructor.
     */
    public function __construct()
    {
        $this->connection = config('safebits_command.connection', 'mysql');
    }
}
