<?php

namespace Safebits\Command\Database\Seeders;

use Illuminate\Support\Facades\Artisan;
use Safebits\Command\Models\Command;

/**
 * Class CommandTableSeeder
 * @package Safebits\Command\Database\Seeders
 */
class CommandTableSeeder extends CMDSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Artisan::all() as $signature => $command) {

            if (property_exists($command, 'scheduled')) {
                $commandExecutions = $command->scheduled ? config('safebits_command.scheduled_commands.' . $signature . '.executions') : [[]];

                if ($commandExecutions) {
                    foreach ($commandExecutions as $commandExecutionParameters) {
                        $this->registerCommandInstance($command->childClassName, $signature, $command->scheduled, $commandExecutionParameters);
                    }
                }
            }
        }
    }

    /**
     * @param $className
     * @param $signature
     * @param $scheduled
     * @param null $parameters
     */
    private function registerCommandInstance($className, $signature, $scheduled, $parameters = null)
    {
        $commandInput = new Command();
        $commandInput->className = $className;
        $commandInput->name = substr($className, strrpos($className, '\\') + 1);
        $commandInput->signature = $signature;
        $commandInput->serverId = 'API01';
        $commandInput->scheduled = $scheduled;
        $commandInput->parameters = count($parameters) > 0 ? json_encode($parameters) : null;
        $commandInput->save();
    }
}
