<?php

namespace Safebits\Command\Database\Seeders;

/**
 * Class DatabaseSeeder
 * @package Safebits\Command\Database\Seeders
 */
class DatabaseSeeder extends CMDSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Eloquent::unguard();

        \Schema::connection($this->connection)->disableForeignKeyConstraints();

        // Add Seeders here
        $this->call(CommandTableSeeder::class);

        \Schema::connection($this->connection)->enableForeignKeyConstraints();
    }
}
