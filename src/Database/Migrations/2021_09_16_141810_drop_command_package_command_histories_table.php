<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Safebits\Command\Database\Migrations\CMDMigration;

/**
 * Class DropCommandPackageCommandHistoriesTable
 */
class DropCommandPackageCommandHistoriesTable extends CMDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('sys_command_history');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('sys_command_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('commandId')->unsigned();
            $table->timestamp('startDate')->useCurrent();;
            $table->timestamp('endDate')->nullable();
            $table->smallInteger('status')->nullable();
            $table->string('message')->nullable();

            $table->foreign('commandId')->references('commandId')->on('sys_commands');
        });
    }
}
