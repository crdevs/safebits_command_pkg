<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Safebits\Command\Database\Migrations\CMDMigration;

/**
 * Class CreateCommandPackageCommandsTable
 */
class CreateCommandPackageCommandsTable extends CMDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('sys_commands', function (Blueprint $table) {
            $table->increments('commandId');
            $table->string('name')->unique();
            $table->string('className')->unique();
            $table->string('signature');
            $table->boolean('isActive')->default(true);
            $table->boolean('isRunning')->default(false);
            $table->boolean('isLogEnabled')->default(true);
            $table->timestamp('lastRunDate')->nullable();
            $table->smallInteger('lastRunStatus')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('sys_commands');
    }
}
