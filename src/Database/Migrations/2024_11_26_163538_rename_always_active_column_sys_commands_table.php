<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameAlwaysActiveColumnSysCommandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sys_commands', function (Blueprint $table) {
            $table->renameColumn('alwaysActive', 'isDefault');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sys_commands', function (Blueprint $table) {
            $table->renameColumn('isDefault', 'alwaysActive');
        });
    }
}
