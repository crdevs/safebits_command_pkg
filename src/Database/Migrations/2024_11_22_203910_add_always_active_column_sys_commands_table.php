<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Safebits\Command\Database\Migrations\CMDMigration;

class AddAlwaysActiveColumnSysCommandsTable extends CMDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sys_commands', function (Blueprint $table) {
            $table->boolean('alwaysActive')->default(false)->after('scheduled');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sys_commands', function (Blueprint $table) {
            $table->dropColumn('alwaysActive');
        });
    }
}
