<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Safebits\Command\Database\Migrations\CMDMigration;

/**
 * Class DropUniqueConstraintsSysCommandsTable
 */
class DropUniqueConstraintsSysCommandsTable extends CMDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sys_commands', function (Blueprint $table) {
            $table->dropUnique(['name']);
            $table->dropUnique(['className']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Note: as previous seeders can create multiple records of a single command, this will remain commented to prevent rollback issues
//        Schema::table('sys_commands', function (Blueprint $table) {
//            $table->unique(['name']);
//            $table->unique(['className']);
//        });
    }
}
