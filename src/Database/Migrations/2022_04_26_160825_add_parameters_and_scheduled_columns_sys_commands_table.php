<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Safebits\Command\Database\Migrations\CMDMigration;

/**
 * Class AddParametersAndScheduledColumnsSysCommandsTable
 */
class AddParametersAndScheduledColumnsSysCommandsTable extends  CMDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sys_commands', function (Blueprint $table) {
            $table->boolean('scheduled')->default(true)->after('halt');
            $table->binary('parameters')->nullable()->default(null)->after('scheduled');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sys_commands', function (Blueprint $table) {
            $table->dropColumn('scheduled');
            $table->dropColumn('parameters');
        });
    }
}
