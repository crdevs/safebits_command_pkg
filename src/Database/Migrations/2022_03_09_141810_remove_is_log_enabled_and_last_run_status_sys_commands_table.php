<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Safebits\Command\Database\Migrations\CMDMigration;

/**
 * Class RemoveIsLogEnabledAndLastRunStatusSysCommandsTable
 */
class RemoveIsLogEnabledAndLastRunStatusSysCommandsTable extends CMDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sys_commands', function (Blueprint $table) {
            $table->dropColumn('isLogEnabled');
            $table->dropColumn('lastRunStatus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sys_commands', function (Blueprint $table) {
            $table->boolean('isLogEnabled')->default(true);
            $table->smallInteger('lastRunStatus')->nullable();
        });
    }
}
