<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Safebits\Command\Database\Migrations\CMDMigration;


class AddExecutionsToleranceSkippedExecutionsSysCommandsTable extends CMDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sys_commands', function (Blueprint $table) {
            $table->integer('executionsTolerance')->default(1)->after('serverId');
            $table->integer('skippedExecutions')->default(0)->after('executionsTolerance');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sys_commands', function (Blueprint $table) {
            $table->dropColumn('executionsTolerance');
            $table->dropColumn('skippedExecutions');
        });
    }
}
