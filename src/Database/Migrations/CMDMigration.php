<?php

namespace Safebits\Command\Database\Migrations;

use Illuminate\Database\Migrations\Migration;

/**
 * Class CMDMigration
 * @package Safebits\Command\Database\Migrations
 */
class CMDMigration extends Migration
{
    /**
     * @var string
     */
    protected $connection;

    /**
     * CreateSystemTable constructor.
     */
    public function __construct()
    {
        $this->connection = config('safebits_command.connection');
    }
}
