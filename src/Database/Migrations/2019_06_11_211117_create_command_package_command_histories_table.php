<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Safebits\Command\Database\Migrations\CMDMigration;

/**
 * Class CreateCommandPackageCommandHistoriesTable
 */
class CreateCommandPackageCommandHistoriesTable extends CMDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_command_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('commandId')->unsigned();
            $table->timestamp('startDate')->useCurrent();;
            $table->timestamp('endDate')->nullable();
            $table->smallInteger('status')->nullable();
            $table->string('message')->nullable();

            $table->foreign('commandId')->references('commandId')->on('sys_commands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_command_history');
    }
}
