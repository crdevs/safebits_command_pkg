<?php

namespace Safebits\Command\Exceptions;

/**
 * Class CommandException
 * @package Safebits\Command\Exceptions
 */
class CommandException extends \Exception
{
    /**
     * CommandException constructor.
     * @param null $statusCode
     * @param null $message
     * @param null $previous
     */
    public function __construct($statusCode = null, $message = null, $previous = null)
    {
        //Set status code as 500 if it doesn't have a previous status code
        if ($statusCode == null) {
            $statusCode = 500;
        }
        parent::__construct($message, $statusCode, $previous);
    }
}
