<?php

namespace Safebits\Command\Exceptions;

/**
 * Class JobProcessingException
 * @package Safebits\Command\Exceptions
 */
class JobProcessingException extends CommandException
{
    /**
     * JobProcessingException constructor.
     * @param $message
     */
    public function __construct($message)
    {
        parent::__construct(500, $message);
    }
}
